import pygame
import os
from pygame.sprite import Sprite

class Bullet(Sprite):
    """Bales disparades desde la nau"""

    def __init__(self, mataMarcianitos, tipus) -> None:
        """Crear un objecte de bala a la posició actual de la nau"""
        super().__init__()
        self.screen = mataMarcianitos.screen
        self.settings = mataMarcianitos.settings

        image_path = os.path.join(self.settings.assets_path, f"laser-{tipus}.png")
        self.image = pygame.image.load(image_path)
        self.rect = self.image.get_rect()
        self.rect.midtop = mataMarcianitos.ship.rect.midtop

        self.y = float(self.rect.y)

    def update(self):
        """Mou la bala"""
        self.y -= self.settings.bullet_speed
        self.rect.y = self.y

    def draw(self):
        """Dibuixa la bala a la posició actual"""
        self.screen.blit(self.image, self.rect)

