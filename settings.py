import os

class Settings:
    """Opcions per MataMarcianitos"""

    def __init__(self) -> None:
        """Inicialitza les opcions"""
        self.screen_width = 1200
        self.screen_height = 800
        self.full_screen = False
        self.screen_title = "Mata marcianitos"
        self.bg_color = (0,0,50)

        self.bullet_speed = 1.0
        self.bullet_width = 3
        self.bullet_height = 15
        self.bullet_color = (255, 60, 60)

        self.vides = 3

        self.millora_speed = 0.1
        self.vida_extra_temps = 15
        self.millora_velocitat_nau_temps = 30
        self.millora_temps_trets_temps = 15
        self.millora_arma_temps = 15

        self.reset()

        self.screen_size = (self.screen_width, self.screen_height)

        self.base_path = os.path.dirname(__file__)
        self.assets_path = os.path.join(self.base_path, "assets")

    def reset(self):
        self.ship_speed = 0.5
        self.enemic_temps = 3
        self.enemic_speed = 0.1
        self.bullets_temps = 1
        self.arma = 1

    def update_dificultat(self, punts):
        if punts == 5:
            self.enemic_speed = 0.2
        elif punts == 10:
            self.enemic_temps = 2
        elif punts == 15:
            self.enemic_speed = 0.3
        elif punts == 20:
            self.enemic_temps = 1
        elif punts == 30:
            self.enemic_speed = 0.4
        elif punts == 50:
            self.enemic_temps = 0.8
        elif punts == 70:
            self.enemic_speed = 0.5
        elif punts == 100:
            self.enemic_temps = 0.5
        elif punts == 120:
            self.enemic_speed = 0.6
        elif punts == 140:
            self.enemic_temps = 0.3
