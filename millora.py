import pygame
import os
from random import randint

from pygame.sprite import Sprite

class Millora(Sprite):
    """Millora"""

    def __init__(self, mataMarcianitos, tipus) -> None:
        """Inicialitzar la millora"""
        super().__init__()
        self.screen = mataMarcianitos.screen
        self.settings = mataMarcianitos.settings

        image_path = os.path.join(self.settings.assets_path, f"raig-{tipus}.png")
        self.image = pygame.image.load(image_path)
        self.rect = self.image.get_rect()

        self.screen_rect = self.screen.get_rect()
        self.rect.y = -self.rect.height
        self.rect.x = randint(0, self.screen_rect.width - self.rect.width)
        self.y = float(self.rect.y)

    def update(self):
        """Mou la millora"""
        self.y += self.settings.millora_speed
        self.rect.y = self.y

    def draw(self):
        """Dibuixa la millora a la posició actual"""
        self.screen.blit(self.image, self.rect)
