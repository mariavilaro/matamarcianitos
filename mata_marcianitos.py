import sys
import pygame
import time

from pygame import mouse

from settings import Settings
from puntuacio import Puntuacio
from button import Button
from ship import Ship
from bullet import Bullet
from enemic import Enemic
from vida_extra import VidaExtra
from millora import Millora

class MataMarcianitos:
    """Classe principal del joc"""

    def __init__(self) -> None:
        """Inicialitza el joc"""
        pygame.init()
        self.settings = Settings()

        if self.settings.full_screen:
            self.screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
        else:
            self.screen = pygame.display.set_mode(self.settings.screen_size)

        pygame.display.set_caption(self.settings.screen_title)

        self.screen_rect = self.screen.get_rect()

        self.puntuacio = Puntuacio(self)
        self.ship = Ship(self)
        self.bullets = pygame.sprite.Group()
        self.enemics = pygame.sprite.Group()
        self.vides_extra = pygame.sprite.Group()
        self.millores_velocitat_nau = pygame.sprite.Group()
        self.millores_temps_trets = pygame.sprite.Group()
        self.millores_arma = pygame.sprite.Group()
        self.play_button = Button(self, "Jugar")
        self.tempsActual = 0
        self.tempsUltimEnemic = 0
        self.tempsUltimTret = 0
        self.jugant = False
        self.disparant = False

    def run_game(self):
        """Loop principal del joc"""
        while True:
            self._check_events()

            if self.jugant:
                self.tempsActual = time.time()
                self._crear_enemic()
                self._crear_vida_extra()
                self._crear_millora_velocitat_nau()
                self._crear_millora_temps_trets()
                self._crear_millora_arma()
                self.ship.update()
                self._update_bullets()
                self._update_enemics()
                self._update_vides_extra()
                self._update_millores_velocitat_nau()
                self._update_millores_temps_trets()
                self._update_millores_arma()

            self._update_screen()

    def _check_events(self):
        """Control d'events de teclat i ratolí"""
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                self._check_play_button(mouse_pos)
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)

    def _check_keydown_events(self, event):
        """Control d'events de teclat: pressionar tecles"""
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = True
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = True
        elif event.key == pygame.K_UP:
            self.ship.moving_up = True
        elif event.key == pygame.K_DOWN:
            self.ship.moving_down = True
        elif event.key == pygame.K_SPACE:
            self.disparant = True
        elif event.key == pygame.K_ESCAPE:
            sys.exit()

    def _check_keyup_events(self, event):
        """Control d'events de teclat: deixar anar tecles"""
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = False
        elif event.key == pygame.K_UP:
            self.ship.moving_up = False
        elif event.key == pygame.K_DOWN:
            self.ship.moving_down = False
        elif event.key == pygame.K_SPACE:
            self.disparant = False

    def _check_play_button(self, mouse_pos):
        """Començar un joc nou quan l'usuari clica Jugar"""
        if not self.jugant and self.play_button.rect.collidepoint(mouse_pos):
            pygame.mouse.set_visible(False)
            self.puntuacio.reset()
            self.settings.reset()
            self.bullets.empty()
            self.enemics.empty()
            self.tempsUltimaVidaExtra = time.time()
            self.tempsUltimaMilloraVelocitatNau = time.time()
            self.tempsUltimaMilloraTempsTrets = time.time() + 10
            self.tempsUltimaMilloraArma = time.time() + 5
            self.vides_extra.empty()
            self.millores_velocitat_nau.empty()
            self.jugant = True

    def _fire_bullet(self):
        """Dispara una nova bala"""
        new_bullet = Bullet(self, self.settings.arma)
        self.bullets.add(new_bullet)

    def _update_bullets(self):
        """Actualitza la posició de les bales i elimina les bales que han sortit de la pantalla"""
        if self.disparant and (self.tempsActual - self.tempsUltimTret > self.settings.bullets_temps):
            print(f"Temps desde ultim tret: {self.tempsActual - self.tempsUltimTret}")
            print(f"Temps entre trets: {self.settings.bullets_temps}")
            self.tempsUltimTret = time.time()
            self._fire_bullet()
        self.bullets.update()
        for bullet in self.bullets.copy():
            if bullet.rect.bottom <= 0:
                self.bullets.remove(bullet)
        collisions = pygame.sprite.groupcollide(
            self.bullets, self.enemics, True, True
        )
        if collisions:
            print(collisions)
            self._enemic_tocat()

    def _crear_enemic(self):
        """Crea una nova nau enemiga"""
        if (self.tempsActual - self.tempsUltimEnemic > self.settings.enemic_temps):
            self.tempsUltimEnemic = time.time()
            nou_enemic = Enemic(self)
            self.enemics.add(nou_enemic)

    def _update_enemics(self):
        """Actualitza la posició de les naus enemigues"""
        self.enemics.update()
        for enemic in self.enemics.copy():
            if enemic.rect.bottom >= self.screen_rect.bottom:
                self._nau_tocada()
                self.enemics.remove(enemic)
        collided = False
        collided = pygame.sprite.spritecollideany(self.ship, self.enemics)
        if collided:
            self.enemics.remove(collided)
            self._nau_tocada()

    def _nau_tocada(self):
        """Actualizar les vides quan un enemic toca la nau"""
        if self.puntuacio.vides > 1:
            self.puntuacio.update_vides(-1)
        else:
            self.puntuacio.update_vides(-1)
            self.jugant = False
            pygame.mouse.set_visible(True)
            print('Game over')

    def _enemic_tocat(self):
        """Actualitzar la puntuacio quan una bala toca a un enemic"""
        self.puntuacio.update_punts(1)
        self.settings.update_dificultat(self.puntuacio.punts)
        print(f"Velocitat enemics: {self.settings.enemic_speed}")
        print(f"Temps entre enemics: {self.settings.enemic_temps}")

    def _crear_vida_extra(self):
        """Crea una nova vida extra"""
        if (self.tempsActual - self.tempsUltimaVidaExtra > self.settings.vida_extra_temps):
            self.tempsUltimaVidaExtra = time.time()
            nova_vida_extra = VidaExtra(self)
            self.vides_extra.add(nova_vida_extra)

    def _crear_millora_velocitat_nau(self):
        """Crea una nova millora de velocitat de la nau"""
        if (self.tempsActual - self.tempsUltimaMilloraVelocitatNau > self.settings.millora_velocitat_nau_temps):
            self.tempsUltimaMilloraVelocitatNau = time.time()
            nova_millora = Millora(self, 3)
            self.millores_velocitat_nau.add(nova_millora)

    def _crear_millora_temps_trets(self):
        """Crea una nova millora de temps entre armes"""
        if self.settings.bullets_temps > 0.39 and (self.tempsActual - self.tempsUltimaMilloraTempsTrets > self.settings.millora_temps_trets_temps):
            self.tempsUltimaMilloraTempsTrets = time.time()
            nova_millora = Millora(self, 2)
            self.millores_temps_trets.add(nova_millora)

    def _crear_millora_arma(self):
        """Crea una nova millora d'arma"""
        if self.settings.arma < 4 and (self.tempsActual - self.tempsUltimaMilloraArma) > self.settings.millora_arma_temps:
            self.tempsUltimaMilloraArma = time.time()
            nova_millora = Millora(self, 1)
            self.millores_arma.add(nova_millora)

    def _update_vides_extra(self):
        """Actualitza la posició de les vides extra"""
        self.vides_extra.update()
        for vida_extra in self.vides_extra.copy():
            if vida_extra.rect.bottom >= self.screen_rect.bottom:
                self.vides_extra.remove(vida_extra)
        collided = False
        collided = pygame.sprite.spritecollideany(self.ship, self.vides_extra)
        if collided:
            self.vides_extra.remove(collided)
            self.puntuacio.update_vides(1)

    def _update_millores_velocitat_nau(self):
        """Actualitza la posició de les millores de velocitat"""
        self.millores_velocitat_nau.update()
        for millora in self.millores_velocitat_nau.copy():
            if millora.rect.bottom >= self.screen_rect.bottom:
                self.millores_velocitat_nau.remove(millora)
        collided = False
        collided = pygame.sprite.spritecollideany(self.ship, self.millores_velocitat_nau)
        if collided:
            self.millores_velocitat_nau.remove(collided)
            self.settings.ship_speed += 0.2
            print(f"Velocitat nau: {self.settings.ship_speed}")

    def _update_millores_temps_trets(self):
        """Actualitza la posició de les millores de temps entre trets"""
        self.millores_temps_trets.update()
        for millora in self.millores_temps_trets.copy():
            if millora.rect.bottom >= self.screen_rect.bottom:
                self.millores_temps_trets.remove(millora)
        collided = False
        collided = pygame.sprite.spritecollideany(self.ship, self.millores_temps_trets)
        if collided:
            self.millores_temps_trets.remove(collided)
            if (self.settings.bullets_temps > 0.39):
                self.settings.bullets_temps -= 0.2
                print(f"Temps entre trets: {self.settings.bullets_temps}")

    def _update_millores_arma(self):
        """Actualitza la posició de les millores d'arma"""
        self.millores_arma.update()
        for millora in self.millores_arma.copy():
            if millora.rect.bottom >= self.screen_rect.bottom:
                self.millores_arma.remove(millora)
        collided = False
        collided = pygame.sprite.spritecollideany(self.ship, self.millores_arma)
        if collided:
            self.millores_arma.remove(collided)
            if (self.settings.arma < 4):
                self.settings.arma += 1

    def _update_screen(self):
        """Redibuixa els elements a la pantalla"""
        self.screen.fill(self.settings.bg_color)
        self.ship.draw()
        for bullet in self.bullets.sprites():
            bullet.draw()
        self.enemics.draw(self.screen)
        self.vides_extra.draw(self.screen)
        self.millores_velocitat_nau.draw(self.screen)
        self.millores_temps_trets.draw(self.screen)
        self.millores_arma.draw(self.screen)
        self.puntuacio.draw()

        if not self.jugant:
            self.play_button.draw()

        pygame.display.flip()


if __name__ == '__main__':
    game = MataMarcianitos()
    game.run_game()
