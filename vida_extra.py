import pygame
import os
from random import randint

from pygame.sprite import Sprite

class VidaExtra(Sprite):
    """Vida extra"""

    def __init__(self, mataMarcianitos) -> None:
        """Inicialitzar la vida extra"""
        super().__init__()
        self.screen = mataMarcianitos.screen
        self.settings = mataMarcianitos.settings

        image_path = os.path.join(self.settings.assets_path, "cor.png")
        self.image = pygame.image.load(image_path)
        self.rect = self.image.get_rect()

        self.screen_rect = self.screen.get_rect()
        self.rect.y = -self.rect.height
        self.rect.x = randint(0, self.screen_rect.width - self.rect.width)
        self.y = float(self.rect.y)

    def update(self):
        """Mou la vida extra"""
        self.y += self.settings.millora_speed
        self.rect.y = self.y

    def draw(self):
        """Dibuixa la vida extra a la posició actual"""
        self.screen.blit(self.image, self.rect)
