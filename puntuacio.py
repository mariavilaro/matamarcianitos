import pygame

class Puntuacio:
    """Puntuacions i vides"""

    def __init__(self, mataMarcianitos) -> None:
        """Inicialitzar puntuacions"""
        self.screen = mataMarcianitos.screen
        self.settings = mataMarcianitos.settings

        self.screen_rect = self.screen.get_rect()

        self.text_color = (255, 255, 255)
        self.font = pygame.font.SysFont(None, 48)

        self.reset()

    def reset(self):
        self.vides = self.settings.vides
        self.punts = 0
        self.update_punts()
        self.update_vides()

    def draw(self):
        self.screen.blit(self.punts_img, self.punts_rect)
        self.screen.blit(self.vides_img, self.vides_rect)

    def update_punts(self, punts = 0):
        self.punts += punts
        punts_str = str(self.punts)
        self.punts_img = self.font.render(punts_str, True, self.text_color, self.settings.bg_color)
        self.punts_rect = self.punts_img.get_rect()
        self.punts_rect.right = self.screen_rect.right - 20
        self.punts_rect.top = 20

    def update_vides(self, vides = 0):
        self.vides += vides
        vides_str = str(self.vides)
        self.vides_img = self.font.render(vides_str, True, self.text_color, self.settings.bg_color)
        self.vides_rect = self.vides_img.get_rect()
        self.vides_rect.left = 20
        self.vides_rect.top = 20
