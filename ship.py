import pygame
import os

class Ship:
    """Nau del jugador"""

    def __init__(self, mataMarcianitos) -> None:
        """Inicialitzar la nau"""
        self.screen = mataMarcianitos.screen
        self.settings = mataMarcianitos.settings

        self.screen_rect = self.screen.get_rect()
        image_path = os.path.join(self.settings.assets_path, "nau.png")
        self.image = pygame.image.load(image_path)
        self.rect = self.image.get_rect()
        self.rect.midbottom = self.screen_rect.midbottom

        self.x = float(self.rect.x)
        self.y = float(self.rect.y)

        self.moving_right = False
        self.moving_left = False
        self.moving_up = False
        self.moving_down = False

    def update(self):
        """Mou la nau"""
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.ship_speed
        if self.moving_left and self.rect.left > self.screen_rect.left:
            self.x -= self.settings.ship_speed
        if self.moving_up and self.rect.top > self.screen_rect.top:
            self.y -= self.settings.ship_speed
        if self.moving_down and self.rect.bottom < self.screen_rect.bottom:
            self.y += self.settings.ship_speed

        self.rect.x = self.x
        self.rect.y = self.y

    def draw(self):
        """Dibuixa la nau a la posició actual"""
        self.screen.blit(self.image, self.rect)
